import asyncio
import sys


class DataStorage:
    """Создаем хранилище для наших метрик"""
    def __init__(self):
        self._data = {}

    def put(self, key, value, timestamp):
        """Метод передает на сервер в хранилище метрики"""
        self._data.setdefault(key,{})
        self._data[key][timestamp] = value

    def get(self, key):
        """Получаем метрики с сервера"""
        data = self._data
        if key != "*":
            data = {
                key: data.get(key, {})
            }
        result = {}
        for key, data_timestamp in data.items():
            result[key] = sorted(data_timestamp.items())
        return result


class ParseError(ValueError):
    pass


class ServerParser:
    """Парсим ответы и запросы"""
    def encode(self, resp):
        """собираем ответ"""
        rows = []
        for response in resp:
            if not response:
                continue
            for key, values in response.items():
                for timestamp, value in values:
                    rows.append(f"{key} {value} {timestamp}")
        result = "ok\n"
        if rows:
            result += "\n".join(rows) + "\n"
        return result + "\n"

    def decode(self, data):
        """Разбираем запрос"""
        parts = data.split("\n")
        commands = []
        for part in parts:
            if not part:
                continue
            try:
                method, params = part.strip().split(" ", 1)
                if method == "put":
                    key, value, timestamp = params.split()
                    commands.append(
                        (method, key, float(value), int(timestamp))
                    )
                elif method == "get":
                    key = params
                    commands.append(
                        (method, key)
                    )
                else:
                    raise ValueError("unknown method")
            except ValueError:
                raise ParseError("wrong command")
        return commands


class ExecutorError(Exception):
    pass


class Executor:
    """Анализируем полученные команды"""
    def __init__(self, storage):
        self.storage = storage

    def run(self, method, *params):
        if method == "put":
            return self.storage.put(*params)
        elif method == "get":
            return self.storage.get(*params)
        else:
            raise ExecutorError("Unsupported method")


class ClientServerProtocol(asyncio.Protocol):
    storage = DataStorage()

    def __init__(self):
        super().__init__()
        self.parser = ServerParser()
        self.executor = Executor(self.storage)
        self._buffer = b''

    def process_data(self, data):
        """Данные для обработки"""
        commands = self.parser.decode(data)
        responses = []
        for command in commands:
            resp = self.executor.run(*command)
            responses.append(resp)
        return self.parser.encode(responses)

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        self._buffer += data
        try:
            decoded_data = self._buffer.decode()
        except UnicodeDecodeError:
            return

        if not decoded_data.endswith('\n'):
            return

        self._buffer = b''
        try:
            resp = self.process_data(decoded_data)
        except (ParseError, ExecutorError) as err:
            self.transport.write(f"error\n{err}\n\n".encode())
            return
        self.transport.write(resp.encode())

def run_server(address='127.0.0.1', port='8888'):
    loop = asyncio.get_event_loop()
    server_coro = loop.create_server(
        ClientServerProtocol,
        address, port
    )

    server = loop.run_until_complete(server_coro)
    host = server.sockets[0].getsockname()
    # print('Serving on {}. Hit CTRL-C to stop'.format(host))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    # print('\nServer shutting down...')
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == '__main__':
    run_server(*sys.argv[1:])
